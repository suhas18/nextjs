import React from "react";
import styled from "styled-components";
import Icon from "../elements/Icon";
import Burger from "./Burger";
import HeaderMenu from "./HeaderMenu";

const Nav = styled.nav`
  width: 100%;
  height: 50px;
  padding: 0 20px;
  display: flex;
  justify-content: space-around;

  .logo {
    padding: 15px 0;
  }
`;

const icon = styled.div`
  height:200px;
  width: 400px;
  position: fixed;
}
`;

const Navbar = () => {
  return (
    <Nav>
      <icon>
        <Icon src={"/beee.png"} width="280" iconText="Connect. Contribute. Conquer." />
      </icon>
      <HeaderMenu />
      <Burger />
    </Nav>
  );
};

export default Navbar;
