import React from "react";
import Button from "./Button";
import List from "./List";
import Link from "../elements/Link";
import Icon from "../elements/Icon";
import TextBlock from "../elements/TextBlock";
import styled from "styled-components";

const type = {
  primary: {
    default: "var(--primary)",
    hover: "var(--textcolor)",
  },
  secondary: {
    default: "var(--secondary)",
    hover: "orange",
  },
  success: {
    default: "var(--success)",
    hover: "lightgreen",
  },
  error: {
    default: "var(--error)",
    hover: "red",
  },
  warning: {
    default: "red",
    hover: "var(--error)",
  },
  transparent: {
    default: "var(--transparent)",
  },
  grey: {
    default: "var(--grey)",
  },
  brown:{
    default:"var(--brown)"
  }
};

const size = {
  sm: {
    default: "var(--small)",
  },
  normal: {
    default: "var(--medium)",
  },
  lg: {
    default: "var(--large)",
  },
};

const Footer = styled.div`
  background-color: ${(props) => type[props.type].default};
  height: calc(${(props) => (size[props.size].default)}*2);
  width: 100%;
`;

Footer.defaultProps = {
  type: "transparent",
  size: "lg",
};

export default function footer({ children, type, size }) {
  return (
    <Footer type={type} size={size}>
        <div
          style={{
            height: 200,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          <h1 style={{ color: "white" }}>Built with ❤️ at GeekyAnts</h1>
          <TextBlock
            title="About"
            text={
              <p>
                In publishing and graphic design,
                <br />
                Lorem ipsum is a placeholder text commonly
                <br />
                relying on meaningful content
              </p>
            }
          ></TextBlock>

          <Button type="secondary" size="sm">
            Hire Engineers
          </Button>
        </div>
    </Footer>
  );
}

/*  height: ${(props) => size[props.size].default}; */
