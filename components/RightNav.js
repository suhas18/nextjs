import React from "react";
import styled from "styled-components";
import Button from "./Button";
import List from "./List";
import Icon from "../elements/Icon";

const Ul = styled.div`
  @media (max-width: 768px) {
    position: fixed;
    transform: ${({ open }) => (open ? "translateY(0)" : "translateY(100%)")};
    right: 0;
    bottom: 0;
    height: 100vh;
    width: 100%;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;
    flex-direction: column;
    margin: 0px;
    background-color: white;
    display: flex;
  }

  @media (min-width: 768px) {
    display: none;
  }
`;

const Div = styled.div`
  display: flex;
  flex-direction:column;
  justify-content: center;
  align-items: center;
`;

const icon = styled.div`
  height:200px;
  width: 400px;
  position: relative;
}
`;

const RightNav = ({ open }) => {
  return (
    <Ul open={open}>
       <Div>
      <icon>
        <Icon src={"/beee.png"} width={280} iconText="Connect. Contribute. Conquer." />
      </icon>
      <List />
     
        <Button type="secondary" size="sm">
          Hire Engineers
        </Button>
      </Div>
    </Ul>
  );
};

export default RightNav;
