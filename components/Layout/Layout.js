import React from "react";
import Header from "../Header";
import Footer from "../Footer";

export default function Layout({ children }) {
  return (
    <>
      <Header size="lg" type="transparent" />
      {children}
      <Footer size="lg" type="brown" />
    </>
  );
}
