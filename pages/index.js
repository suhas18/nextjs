import Button from "../components/Button";
import Layout from "../components/Layout/Layout";
// import Banner from "../components/Banner";
// import TextBlock from "../elements/TextBlock";
// import Card from "../components/Card";
// import Icon from "../elements/Icon";




// const bannerBlockStyle = {
//   borderColor: "white",
//   borderWidth: 10,
//   height: 500,
//   width: 300,
//   display: "flex",
//   justifyContent: "center",
//   alignItems: "center",
// };

export default function Home() {
  return (
    <div
      className="main"
      style={{
        backgroundColor: "white",
        height: "190vh",
      }}
    >
      <div
        className="div1"
        style={{
          height: "145vh",
          backgroundImage: "url(/bg.jpg)",
        }}
      >
        <Layout>
           <div
            style={{
              height: "100vh",
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
             <div
              style={{
                display: "flex",
                flexDirection: "column",
                height: "90vh",
                justifyContent: "space-evenly",
                alignItems: "center",
                padding: "3rem",
              }}
            >
              <h1
                style={{
                  color: "#341907",
                  fontSize: "4.325rem",
                  textAlign: "center",
                }}
              >
                A Hive of Best Remote IT Talent
              </h1>
              <text
                style={{
                  color: "#341907",
                  fontSize: "2.1875rem",
                  textAlign: "center",
                }}
              >
                Hire pre-screened engineers on a contractual basis today!
              </text>
              <Button type="secondary" size="lg" href="https://www.google.com">
                Hire Engineers
              </Button>
            </div>
            <img
              className="image"
              src={"/people.png"}
              style={{
                marginRight: 200,
                height: 550,
                marginBottom: 200,
              }}
            />
          </div> 
           {/* <div
            style={{
              height: "15vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
            }}
          >
            <Banner type="brown" size="sm" className="banner">
              <div style={bannerBlockStyle}>
                <TextBlock title="3000+" text="Happy Clients"></TextBlock>
              </div>
              <div style={bannerBlockStyle}>
                <TextBlock title="10,000+" text="Developers"></TextBlock>
              </div>
              <div style={bannerBlockStyle}>
                <TextBlock
                  title="1000+ "
                  text="Verified talented Partners"
                ></TextBlock>
              </div>
              <div style={bannerBlockStyle}>
                <TextBlock title="250+ " text="To hire Engineers"></TextBlock>
              </div>
            </Banner>
          </div>  */}

           {/* <div
            style={{
              marginTop: 100,
              display: "flex",
              justifyContent: "space-evenly",
              alignItems:"center",
              marginBottom: 50,
            }}
            className="cards"
          >
            <Card type="grey" size="normal">
              <Icon src={"/card1.png"}></Icon>
              <text
                style={{
                  color: "black",
                  fontSize: 20,
                  padding: 20,
                  textAlign: "center",
                }}
              >
                Open option to discern the hefty pool of Software Developers and
                Leads.
              </text>
            </Card>

            <Card type="grey" size="normal">
              <Icon src={"/card3.png"}></Icon>
              <text
                style={{
                  color: "black",
                  fontSize: 20,
                  padding: 20,
                  textAlign: "center",
                }}
              >
                Engage in global projects and clients on a remote basis.
              </text>
            </Card>

            <Card type="grey" size="normal">
              <Icon src={"/card1.png"}></Icon>
              <text
                style={{
                  color: "black",
                  fontSize: 20,
                  padding: 20,
                  textAlign: "center",
                }}
              >
                Broad-cast your ideas and tech stacks that maneuvers the
                business.
              </text>
             </Card>
          </div>  */}
        </Layout>
      </div>
    </div>
  );
}
