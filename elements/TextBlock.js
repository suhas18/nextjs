import React from "react";

export default function TextBlock({ title, text }) {


  return (
    <div
      style={{
        flexDirection: "column",
      }}
    >
      <h1
        style={{
          color: "white",
          fontWeight: "bold",
          fontSize: 30,
          letterSpacing: 2,
          margin: 3,
        }}
      >
        {title}
      </h1>
      <text
        style={{
          color: "white",
          letterSpacing: 1,
        }}
      >
        {text}
      </text>
    </div>
  );
}
