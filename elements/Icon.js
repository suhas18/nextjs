import React from 'react'

export default function Icon({src,iconText,width}) {
  return (
    <div style={{
        display:"flex",
        flexDirection:"column",
        justifyContent:"center",
        alignItems:"center"
    }}>
         <img
            src={src} width={width} />
            <text style={{color:"black",fontSize:15,marginLeft:40,color:"#341907"}}>{iconText}</text>
    </div>
  )
}
